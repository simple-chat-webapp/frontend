import React from "react";
import ReactDOM from "react-dom/client";
import App from "./components/App";
// import { FirebaseAppProvider } from "reactfire";
// import { firebaseClient } from "./firebase";

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/firebase-messaging-sw.js")
    .then((registration) => {
      console.log("Service Worker registered with scope:", registration.scope);
    })
    .catch((error) => {
      console.error("Service Worker registration failed:", error);
    });
}

ReactDOM.createRoot(document.getElementById("root")).render(
  // <FirebaseAppProvider firebaseConfig={firebaseClient}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  // </FirebaseAppProvider>
);
