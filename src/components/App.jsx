import { useEffect } from "react";
import { firebaseClient } from "../firebase";
import { Routers } from "../config";
import "bootstrap/dist/css/bootstrap.css";


function App() {
  useEffect(() => {
    firebaseClient();
  }, []);

  return (
    <div className="bg-success">
      <Routers />
    </div>
  );
}

export default App;
