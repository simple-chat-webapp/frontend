import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyBnsd-g0vA1WcZe3dR30dEv7zvTS1XgLkg",
  authDomain: "final-project-pos.firebaseapp.com",
  databaseURL: "https://final-project-pos-default-rtdb.firebaseio.com",
  projectId: "final-project-pos",
  storageBucket: "final-project-pos.appspot.com",
  messagingSenderId: "16247965628",
  appId: "1:16247965628:web:1fbbc1a9a1370c00428db9",
  measurementId: "G-6CBHR6HQH1",
};

const app = initializeApp(firebaseConfig);
const messaging = getMessaging(app);

function firebaseClient() {
  Notification.requestPermission().then((permission) => {
    if (permission === "granted") {
      console.log("Notification permission granted");
      getToken(messaging, {
        vapidKey:
          "BIL4A0tnMAxtNPzNKG_jVGEN8qttx1lniXtoy9v2CJeRTkrIPo0u3oCfFd46fsjdLIk90vSktER8tuHiZBtU318",
      })
        .then((currentToken) => {
          if (currentToken) {
            console.log("Current Token:", currentToken);
          } else {
            console.log("No registration token available.");
          }
        })
        .catch((err) => {
          console.error("Error getting messaging token:", err);
        });
    } else {
      console.error("Notification permission denied");
    }
  });
}

export { firebaseClient };
