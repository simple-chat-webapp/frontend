// Import Firebase from a CDN in non-module mode
importScripts("https://www.gstatic.com/firebasejs/9.6.0/firebase-app-compat.js");
importScripts("https://www.gstatic.com/firebasejs/9.6.0/firebase-messaging-compat.js");

// importScripts("./lib/firebase-app-compat.js");
// importScripts("./lib/firebase-messaging-compat.js");

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBnsd-g0vA1WcZe3dR30dEv7zvTS1XgLkg",
  authDomain: "final-project-pos.firebaseapp.com",
  databaseURL: "https://final-project-pos-default-rtdb.firebaseio.com",
  projectId: "final-project-pos",
  storageBucket: "final-project-pos.appspot.com",
  messagingSenderId: "16247965628",
  appId: "1:16247965628:web:1fbbc1a9a1370c00428db9",
  measurementId: "G-6CBHR6HQH1",
};

// Initialize the Firebase App in the service worker
firebase.initializeApp(firebaseConfig);

// Initialize the Firebase Messaging service in the service worker
const messaging = firebase.messaging();

// Handle background messages
messaging.onBackgroundMessage((payload) => {
  console.log("Received background message:", payload);

  // Customize the behavior as needed

  // Return an empty response or a Promise that resolves to an empty response
  //   return self.registration.showNotification("Notification Title", {
  //     body: "Notification Body",
  //   });
});

// Handle notification click event
self.addEventListener("notificationclick", (event) => {
  event.notification.close();
  // Add your custom logic for handling the notification click
  console.log("Notification clicked:", event);
});

// Handle notification close event
self.addEventListener("notificationclose", (event) => {
  // Add your custom logic for handling the notification close
  console.log("Notification closed:", event);
});
